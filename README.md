# TESTS of Pesel Validation API

### What is PESEL?
___
PESEL (Universal Electronic System for Registration of the Population) is a social security number in Poland.
It always has 11 digits, identifies just one person and cannot be changed to another one.
##### It contains data about:
- date of birth
- sex
- consecutive number
- control number

Every digit has it's meaning. The pattern looks like:

**RRMMDDPPPPK**

**RR** - two last digits of birth year 

**MM** - month of birth

**DD** - birth-day

**PPPP** - consecutive number. Last digit is an indication of gender (female - even number, male - odd number)

**K** - control number

#### [More](https://en.wikipedia.org/wiki/PESEL) information about structure of PESEL number.

### About my project
___
- [Rest API](https://peselvalidatorapitest.azurewebsites.net/swagger/index.html) used to tests
- Maven project with Java
- TestNG
- Rest Assured
- test class in convention **BDD given().when().then**

This project was developed during my postgraduate studies at AGH University (quality assurance) 
to complete subject: automation.

Project contains automated tests of endpoint GET /api/Pesel
 - coverage of PESEL structure requirements
 - coverage of all error codes of REST service

An additional requirement to complete the subject was at least one automated test of public REST API.
To fulfill this requirement I chose [Cat Facts! REST API](https://cat-fact.herokuapp.com/#/).
