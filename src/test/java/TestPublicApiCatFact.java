import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

public class TestPublicApiCatFact {
    @Test
    public void testGetRequest_StatusCode() {
        Response responce = get("https://cat-fact.herokuapp.com/facts/random?animal_type=cat&amount=1");
        Assert.assertEquals(responce.statusCode(), 200);

        String type = responce.path("type");
        Assert.assertEquals(type, "cat");
    }

    @Test
    public void testGetRequest_TheFact() {
        Response response = get("https://cat-fact.herokuapp.com/facts/591f98803b90f7150a19c23e");

        String expectedFactText = "Cats come back to full alertness from the sleep state faster than any other creature.";
        String actualText = response.path("text");

        Assert.assertEquals(actualText, expectedFactText);
    }
}
