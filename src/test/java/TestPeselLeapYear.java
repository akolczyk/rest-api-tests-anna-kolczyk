import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

public class TestPeselLeapYear extends BasePeselTest {
    @Test
    public void testGetRequest_ValidLeapYear() {
        Response response = get("/Pesel?pesel=92022925566");

        String expectedBody = "{\"pesel\":\"92022925566\",\"isValid\":true,\"birthDate\":" +
                "\"1992-02-29T00:00:00\",\"sex\":\"Female\",\"errors\":[]}";

        String actualBody = response.getBody().asString();
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void testGetRequest_InvalidLeapYear() {
        Response response = get("/Pesel?pesel=85022910126");

        String actualResponse = response.path("errors[0].errorCode");
        Assert.assertEquals(actualResponse, "INVD");

        String errorMsg = response.path("errors[0].errorMessage");
        Assert.assertEquals(errorMsg, "Invalid day.");
    }

    @Test
    public void testGetRequest_ValidLeapYear30Feb() {
        Response response = get("/Pesel?pesel=92023001227");

        String actualResponse = response.path("errors[0].errorCode");
        Assert.assertEquals(actualResponse, "INVD");

        String errorMsg = response.path("errors[0].errorMessage");
        Assert.assertEquals(errorMsg, "Invalid day.");
    }
}
