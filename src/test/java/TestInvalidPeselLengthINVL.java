import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

public class TestInvalidPeselLengthINVL extends BasePeselTest {
    @Test
    public void testGetRequest_ResponseErrorCode_12Characters() {
        Response response = get("/Pesel?pesel=790815716351");

        String actualResponse = response.path("errors[0].errorCode");
        Assert.assertEquals(actualResponse, "INVL");

        String errorMsg = response.path("errors[0].errorMessage");
        Assert.assertEquals(errorMsg, "Invalid length. Pesel should have exactly 11 digits.");
    }

    @Test
    public void testGetRequest_ResponseErrorCode_10Characters() {
        Response response = get("/Pesel?pesel=7908157163");

        String actualResponse = response.path("errors[0].errorCode");
        Assert.assertEquals(actualResponse, "INVL");

        String errorMsg = response.path("errors[0].errorMessage");
        Assert.assertEquals(errorMsg, "Invalid length. Pesel should have exactly 11 digits.");
    }
}
