import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

public class TestInvalidPeselInvalidDate extends BasePeselTest {
    @Test
    public void testGetRequest_InvalidDay31june() {
        Response response = get("/Pesel?pesel=90063102119");

        String actualResponse = response.path("errors[0].errorCode");
        Assert.assertEquals(actualResponse, "INVD");

        String errorMsg = response.path("errors[0].errorMessage");
        Assert.assertEquals(errorMsg, "Invalid day.");
    }

    @Test
    public void testGetRequest_InvalidDay0january() {
        Response response = get("/Pesel?pesel=90010011112");

        String actualResponse = response.path("errors[0].errorCode");
        Assert.assertEquals(actualResponse, "INVD");

        String errorMsg = response.path("errors[0].errorMessage");
        Assert.assertEquals(errorMsg, "Invalid day.");
    }

    @Test
    public void testGetRequest_InvalidMonth13_AndYear() {
        Response response = get("/Pesel?pesel=85130101438");

        String actualYear = response.path("errors[0].errorCode");
        Assert.assertEquals(actualYear, "INVY");

        String errorMsgYear = response.path("errors[0].errorMessage");
        Assert.assertEquals(errorMsgYear, "Invalid year.");

        String actualMonth = response.path("errors[1].errorCode");
        Assert.assertEquals(actualMonth, "INVM");

        String errorMsgMonth = response.path("errors[1].errorMessage");
        Assert.assertEquals(errorMsgMonth, "Invalid month.");
    }

    @Test
    public void testGetRequest_InvalidMonth0_AndYear() {
        Response response = get("/Pesel?pesel=90001512147");

        String actualYear = response.path("errors[0].errorCode");
        Assert.assertEquals(actualYear, "INVY");

        String errorMsgYear = response.path("errors[0].errorMessage");
        Assert.assertEquals(errorMsgYear, "Invalid year.");

        String actualMonth = response.path("errors[1].errorCode");
        Assert.assertEquals(actualMonth, "INVM");

        String errorMsgMonth = response.path("errors[1].errorMessage");
        Assert.assertEquals(errorMsgMonth, "Invalid month.");
    }

    @Test
    public void testGetRequest_InvalidDayMonthYear() {
        Response response = get("/Pesel?pesel=45936781269");

        String year = response.path("errors[0].errorCode");
        Assert.assertEquals(year, "INVY");

        String errorMsgYear = response.path("errors[0].errorMessage");
        Assert.assertEquals(errorMsgYear, "Invalid year.");

        String month = response.path("errors[1].errorCode");
        Assert.assertEquals(month, "INVM");

        String errorMsgMonth = response.path("errors[1].errorMessage");
        Assert.assertEquals(errorMsgMonth, "Invalid month.");

        String day = response.path("errors[2].errorCode");
        Assert.assertEquals(day, "INVD");

        String errorMsgDay = response.path("errors[2].errorMessage");
        Assert.assertEquals(errorMsgDay, "Invalid day.");
    }
}
