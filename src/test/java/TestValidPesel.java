import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;

public class TestValidPesel extends BasePeselTest {
    @Test
    public void testGetRequest_ResponseStatusCodeOk() {
        Response response = get("/Pesel?pesel=99072661383");
        Assert.assertEquals(response.statusCode(), 200);
    }

    @Test
    public void testGetResponse_XIXcentury_IsValidBirthDate() {
        Response response = get("/Pesel?pesel=90860610204");
        Assert.assertTrue(response.path("isValid"));

        String birthdate = response.path("birthDate");
        Assert.assertEquals(birthdate, "1890-06-06T00:00:00");
    }

    @Test
    public void testGetRequest_XXcentury_BodyAndBirthDate() {
        Response response = get("/Pesel?pesel=99072661383");

        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"99072661383\",\"isValid\":true,\"birthDate\":\"1999-07-26T00:00:00\"," +
                "\"sex\":\"Female\",\"errors\":[]}";

        Assert.assertEquals(actualBody, expectedBody);

        String birthdate = response.path("birthDate");
        Assert.assertEquals(birthdate, "1999-07-26T00:00:00");
    }

    @Test
    public void testGetResponse_XXIcentury_IsValidBirthDate() {
        Response response = get("/Pesel?pesel=00290247954");
        Assert.assertTrue(response.path("isValid"));

        String birthdate = response.path("birthDate");
        Assert.assertEquals(birthdate, "2000-09-02T00:00:00");
    }

    @Test
    public void testGetResponse_CheckSexF() {
        Response response = get("/Pesel?pesel=15313098464");

        String actualResponse = response.path("sex");
        Assert.assertEquals(actualResponse, "Female");
    }

    @Test
    public void testGetResponse_CheckSexM() {
        Response response = get("/Pesel?pesel=15313089158");

        String actualResponse = response.path("sex");
        Assert.assertEquals(actualResponse, "Male");
    }
}
