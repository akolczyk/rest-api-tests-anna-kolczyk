import org.testng.annotations.Test;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.*;

public class TestInvalidPeselCheckSumINVC_bdd extends BasePeselTest {
    @Test
    public void testGetRequest_ResponseElements_CheckSum() {
        String expectedError = "INVC";
        String expectedErrorMsg = "Check sum is invalid. Check last digit.";

        when()
                .get("/Pesel?pesel=78052174852")
                .then()
                .body("errors[0].errorCode", is(expectedError))
                .and()
                .body("errors[0].errorMessage", is(expectedErrorMsg));
    }
}
