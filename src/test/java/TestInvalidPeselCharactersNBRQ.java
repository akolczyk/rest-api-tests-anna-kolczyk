import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

public class TestInvalidPeselCharactersNBRQ extends BasePeselTest {

    @Test
    public void testGetRequest_CharacterErrorCode_NBRQ() {
        Response response = get("/Pesel?pesel=pese1%21%40%23%24%25%5E");

        String actualErrorCode = response.path("errors[0].errorCode");
        Assert.assertEquals(actualErrorCode, "NBRQ");

        String errorMsg = response.path("errors[0].errorMessage");
        Assert.assertEquals(errorMsg, "Invalid characters. Pesel should be a number.");
    }
}
